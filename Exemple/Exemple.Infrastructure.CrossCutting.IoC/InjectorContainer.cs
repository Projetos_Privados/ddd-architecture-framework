﻿using Exemple.Domain.AggregateModels;
using Exemple.Domain.CommandHandlers;
using Exemple.Domain.Commands;
using Exemple.Domain.Interfaces.Queries;
using Exemple.Domain.Interfaces.Repositories;
using Exemple.Domain.Queries;
using Exemple.Infrastructure.Data.Context;
using Exemple.Infrastructure.Data.Repository;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using BaseIoC = Base.Infrastructure.CrossCutting.IoC;

namespace Exemple.Infrastructure.CrossCutting.IoC {
    public class InjectorContainer {

        public static void Register( IServiceCollection services, string connectionString ) {

            #region [ Context ]
            
            BaseIoC.InjectorContainer.InjectContext<ExempleContext>( services, connectionString );

            #endregion [ Context ]

            #region [ Repository ]

            services.AddScoped<IWeatherForecastRepository, WeatherForecastRepository>( );
            
            #endregion [ Repository ]
            
            #region [ Query ]
            
            services.AddScoped<IWeatherForecastQuery, WeatherForecastQuery>( );

            #endregion [ Query ]

            #region [ Command ]

            services.AddScoped<IRequestHandler<PostWeatherForecastCommand, WeatherForecast>, PostWeatherForecastCommandHandler>( );

            #endregion [ Command ]
        }
    }
}
