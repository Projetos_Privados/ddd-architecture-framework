﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using Xunit.DependencyInjection;
using BaseIoC = Base.Infrastructure.CrossCutting.IoC;
using ExempleIoC = Exemple.Infrastructure.CrossCutting.IoC;

[assembly: TestFramework( "Exemple.Domain.UnitTest.IoC.StartupTest", "Exemple.Domain.UnitTest" )]

namespace Exemple.Domain.UnitTest.IoC {
    public class StartupTest: DependencyInjectionTestFramework {
        public IServiceProvider ServiceProvider { get; private set; }

        public StartupTest( IMessageSink messageSink ) : base( messageSink ) {
        }

        protected override void ConfigureServices( IServiceCollection services ) {
            BaseIoC.InjectorContainer.Register( services, null, null );
            ExempleIoC.InjectorContainer.Register( services, null );
            ServiceProvider = services.BuildServiceProvider( );
        }
    }
}
