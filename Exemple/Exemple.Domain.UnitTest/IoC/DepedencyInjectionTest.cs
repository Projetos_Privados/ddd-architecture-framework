﻿using Base.Test.Reflection;
using System;
using Xunit;

namespace Exemple.Domain.UnitTest.IoC {
    public class DependencyInjectionTest {
        private readonly IServiceProvider _serviceProvider;

        public DependencyInjectionTest( IServiceProvider serviceProvider ) {
            _serviceProvider = serviceProvider;
        }

        [Fact]
        public void Repository_interfaces_must_be_injected( ) {
            InjectedInterfaces.Verify( _serviceProvider, "Exemple.Domain", "Exemple.Domain.Interfaces.Repositories" );
        }

        [Fact]
        public void Query_interfaces_must_be_injected( ) {
            InjectedInterfaces.Verify( _serviceProvider, "Exemple.Domain", "Exemple.Domain.Interfaces.Queries" );
        }
    }
}
