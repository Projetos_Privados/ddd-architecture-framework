﻿using Exemple.Domain.Interfaces.Queries;
using Exemple.Domain.Interfaces.Repositories;
using Exemple.Domain.UnitTest.Commands.WeatherForecasts.Base;
using Exemple.Infrastructure.Data.Context;
using System.Threading.Tasks;
using Exemple.Domain.AggregateModels;
using System;
using Xunit;
using Exemple.Domain.Commands;
using Exemple.Domain.CommandHandlers;
using System.Threading;

namespace Exemple.Domain.UnitTest.Commands.WeatherForecasts.Exemple {
    public class PostWeatherForecastTest: WeatherForecastTest {
        public PostWeatherForecastTest( ExempleContext context, IWeatherForecastRepository weatherForecastRepository, IWeatherForecastQuery weatherForecastQuery ) 
            : base( context, weatherForecastRepository, weatherForecastQuery ) {
        }

        private async Task Mock( ) {
            await LimparContexto( );

            var weatherForecast = ( await _context.WeatherForecasts.AddAsync( new WeatherForecast( DateTime.Now, 25, "Hot" ) ) ).Entity;

            await SalvarContexto( );
        }

        [Fact]
        public async Task Weather_forecast_must_be_created( ) {
            await Mock( );

            var command = new PostWeatherForecastCommand( 
                DateTime.Now, 
                10, 
                "Freezing" );

            var handler = new PostWeatherForecastCommandHandler(
                _bus,
                _domainNotificationHandler,
                _weatherForecastQuery,
                _weatherForecastRepository );

            var result = await handler.Handle( command, CancellationToken.None );

            Assert.NotNull( result );
        }
    }
}
