﻿using Base.Test.Commands;
using Exemple.Domain.Interfaces.Queries;
using Exemple.Domain.Interfaces.Repositories;
using Exemple.Infrastructure.Data.Context;

namespace Exemple.Domain.UnitTest.Commands.WeatherForecasts.Base {

    public class WeatherForecastTest: CommandTestBase {
        protected readonly ExempleContext _context;
        protected readonly IWeatherForecastRepository _weatherForecastRepository;
        protected readonly IWeatherForecastQuery _weatherForecastQuery;

        public WeatherForecastTest( ExempleContext context, IWeatherForecastRepository weatherForecastRepository, IWeatherForecastQuery weatherForecastQuery )
            : base( context ) {
            _context = context;
            _weatherForecastRepository = weatherForecastRepository;
            _weatherForecastQuery = weatherForecastQuery;
        }
    }
}
