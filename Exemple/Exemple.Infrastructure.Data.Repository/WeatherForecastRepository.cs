﻿using Base.Infrastructure.Data.Repository.Repositories;
using Exemple.Domain.AggregateModels;
using Exemple.Domain.Interfaces.Repositories;
using Exemple.Infrastructure.Data.Context;

namespace Exemple.Infrastructure.Data.Repository {
    public class WeatherForecastRepository: ReadWriteRepository<WeatherForecast>, IWeatherForecastRepository {
        public WeatherForecastRepository( ExempleContext context ) : base( context ) { }
    }
}
