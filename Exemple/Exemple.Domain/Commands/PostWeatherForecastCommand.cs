﻿using Base.Domain.Commands;
using Exemple.Domain.AggregateModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exemple.Domain.Commands {
    public class PostWeatherForecastCommand: Command<WeatherForecast> {
        public DateTime Date { get; set; }
        public int TemperatureC { get; set; }
        public string Summary { get; set; }

        public PostWeatherForecastCommand( DateTime date, int temperatureC, string summary ) {
            Date = date;
            TemperatureC = temperatureC;
            Summary = summary;
        }
    }
}
