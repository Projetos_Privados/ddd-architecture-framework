﻿using Base.Domain.Interfaces;
using Exemple.Domain.AggregateModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exemple.Domain.Specifications {
    public static class WeatherForecastSpecification {
        public static ISpecification<WeatherForecast> WithId( this ISpecification<WeatherForecast> spec, long id ) => 
            spec.And( x => x.WeatherForecastId == id );
    }
}
