﻿using Base.Domain.CommandHandlers;
using Base.Domain.Events;
using Base.Domain.Interfaces;
using Exemple.Domain.AggregateModels;
using Exemple.Domain.Commands;
using Exemple.Domain.Interfaces.Queries;
using Exemple.Domain.Interfaces.Repositories;
using Exemple.Domain.Validations.Commands.Exemple;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Exemple.Domain.CommandHandlers {
    public class PostWeatherForecastCommandHandler: CommandHandler<PostWeatherForecastCommand, WeatherForecast> {

        private readonly IWeatherForecastRepository _weatherForecastRepository;

        public PostWeatherForecastCommandHandler(
            IMediatorHandler bus,
            INotificationHandler<DomainNotification> notificationHandler,
            IWeatherForecastQuery weatherForecastQuery,
            IWeatherForecastRepository weatherForecastRepository )
            : base( bus, notificationHandler,
                  new GetWeatherForecastCommandValidation(
                      weatherForecastQuery ) ) {
            _weatherForecastRepository = weatherForecastRepository;
        }

        public override async Task<WeatherForecast> Handle( PostWeatherForecastCommand command, CancellationToken cancellationToken ) {
            var valid = await IsValidAsync( command, cancellationToken );

            if ( !valid )
                return null;

            var weatherForecast = new WeatherForecast(
                command.Date,
                command.TemperatureC,
                command.Summary
                );

            _weatherForecastRepository.Add( weatherForecast );

            await _weatherForecastRepository.SaveChangesAsync( cancellationToken );

            return weatherForecast;
        }
    }
}
