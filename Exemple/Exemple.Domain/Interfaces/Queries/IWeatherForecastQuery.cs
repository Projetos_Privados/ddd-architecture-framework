﻿using Exemple.Domain.AggregateModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Exemple.Domain.Interfaces.Queries {
    public interface IWeatherForecastQuery {
        Task<bool> ExistsAsync( long id, CancellationToken cancellationToken );

        ValueTask<WeatherForecast> GetAsync( long id, CancellationToken cancellationToken );

        Task<List<WeatherForecast>> GetAsync( CancellationToken cancellationToken );
    }
}
