﻿using Base.Domain.Interfaces;
using Exemple.Domain.AggregateModels;

namespace Exemple.Domain.Interfaces.Repositories {
    public interface IWeatherForecastRepository: IReadWriteRepository<WeatherForecast> {
    }
}
