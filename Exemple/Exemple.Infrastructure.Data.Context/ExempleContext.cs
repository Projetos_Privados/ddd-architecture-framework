﻿using Base.Infrastructure.Data.Context;
using Exemple.Domain.AggregateModels;
using Exemple.Infrastructure.Data.Context.Mappings;
using Microsoft.EntityFrameworkCore;
using System;

namespace Exemple.Infrastructure.Data.Context {
    public class ExempleContext: BaseContext {
        public ExempleContext( DbContextOptions options ) : base( options ) { }

        public DbSet<WeatherForecast> WeatherForecasts { get; private set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder ) {

            modelBuilder
                .ApplyConfiguration( new WeatherForecastMap( ) );

            base.OnModelCreating( modelBuilder );
        }
    }
}
