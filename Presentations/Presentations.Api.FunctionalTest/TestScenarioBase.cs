using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using Presentations.Api;
using Presentations.Api.Application.ViewModels;
using Presentations.Api.FunctionalTest.Extensions;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Presentations.Api.FunctionalTest {
    public abstract class TestScenarioBase: IClassFixture<WebApplicationFactory<BaseStartup>> {
        protected readonly string ApiUrl;
        protected readonly WebApplicationFactory<BaseStartup> _factory;
        protected readonly HttpClient _client;
        private readonly ITestOutputHelper _output;

        public TestScenarioBase(
            WebApplicationFactory<BaseStartup> factory,
            string apiUrl,
            ITestOutputHelper output ) {
            Environment.SetEnvironmentVariable( "IS_TESTE", "true" );
            // Arrange
            ApiUrl = apiUrl;
            _factory = factory;
            _client = _factory.CreateClient( );
            _output = output;
        }

        protected async Task EnsureSuccessAsync( HttpResponseMessage response ) {
            switch ( response.StatusCode ) {
                case HttpStatusCode.InternalServerError:
                case HttpStatusCode.BadRequest:
                    await LogResponseContent<ErrorValidationResponse>( response );
                    break;
                default:
                    break;
            }
            response.EnsureSuccessStatusCode( );
        }

        private async Task LogResponseContent<T>( HttpResponseMessage response ) {
            var content = await response.GetResponseAsync<T>( );
            _output.WriteLine( JsonConvert.SerializeObject( content ) );
        }
    }
}
