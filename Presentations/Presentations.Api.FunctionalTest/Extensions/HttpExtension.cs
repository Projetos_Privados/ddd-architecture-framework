﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Presentations.Api.FunctionalTest.Extensions {
    public static class HttpExtension {
        public static async Task<T> GetResponseAsync<T>(this HttpResponseMessage httpResponse){
            var resultContent = await httpResponse.Content.ReadAsStringAsync( );
            return JsonConvert.DeserializeObject<T>( resultContent );
        }

        public static async Task<HttpResponseMessage> PostAsync<T>(this HttpClient client, string requestUri, T content ) {
            var serializedBody = new StringContent( JsonConvert.SerializeObject( content ), Encoding.UTF8, "application/json" );
            return await client.PostAsync( requestUri, serializedBody );
        }
    }
}
