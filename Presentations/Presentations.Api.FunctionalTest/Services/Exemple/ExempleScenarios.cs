﻿using Microsoft.AspNetCore.Mvc.Testing;
using Presentations.Api;
using Xunit.Abstractions;

namespace Presentations.Api.FunctionalTest.Services.Exemple {
    public class ExempleScenarios: TestScenarioBase {
        public ExempleScenarios( WebApplicationFactory<BaseStartup> factory, ITestOutputHelper output )
            : base( factory, "api/Exemple", output ) {
        }
    }
}
