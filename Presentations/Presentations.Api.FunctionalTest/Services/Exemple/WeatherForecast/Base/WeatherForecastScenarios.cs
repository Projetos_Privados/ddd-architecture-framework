﻿using Microsoft.AspNetCore.Mvc.Testing;
using Presentations.Api;
using Xunit.Abstractions;

namespace Presentations.Api.FunctionalTest.Services.Exemple.Base {
    public class WeatherForecastScenarios: ExempleScenarios {

        protected string EndpointUrl { get; set; } = "WeatherForecast";

        public WeatherForecastScenarios( WebApplicationFactory<BaseStartup> factory, ITestOutputHelper output )
            : base( factory, output ) {
        }
    }
}
