﻿using Microsoft.AspNetCore.Mvc.Testing;
using Presentations.Api;
using Presentations.Api.Application.ViewModels.Exemple;
using Presentations.Api.FunctionalTest.Extensions;
using Presentations.Api.FunctionalTest.Services.Exemple.Base;
using System;
using Xunit;
using Xunit.Abstractions;

namespace Presentations.Api.FunctionalTest.Services.Exemple.WeatherForecast {
    public class PostWeatherForecastScenarios: WeatherForecastScenarios {
        public PostWeatherForecastScenarios( WebApplicationFactory<BaseStartup> factory, ITestOutputHelper output ) 
            : base( factory, output ) {
        }

        [Fact]
        public async void Post_weather_forecast_ok( ) {

            #region [ Arrange ]

            var date = DateTime.Now;
            var temperature = 25;
            var summary = "Hot";
            var request = new PostWeatherForecastViewModel( date, temperature, summary );

            #endregion [ Arrange ]

            #region [ Act ]

            var response = await _client.PostAsync( $"{ApiUrl}/{EndpointUrl}", request );

            #endregion [ Act ]

            #region [ Assert ]

            await EnsureSuccessAsync( response );
            var weatherForecast = await response.GetResponseAsync<WeatherForecastViewModel>( );

            Assert.NotNull( weatherForecast );

            #endregion [ Assert ]
        }
    }
}
