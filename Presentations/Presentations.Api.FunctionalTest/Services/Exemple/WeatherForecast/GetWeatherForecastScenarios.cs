﻿using Microsoft.AspNetCore.Mvc.Testing;
using Presentations.Api.Application.ViewModels.Exemple;
using Presentations.Api.FunctionalTest.Extensions;
using Presentations.Api.FunctionalTest.Services.Exemple.Base;
using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;

namespace Presentations.Api.FunctionalTest.Services.Exemple.WeatherForecast {
    public class GetWeatherForecastScenarios: WeatherForecastScenarios {
        public GetWeatherForecastScenarios( WebApplicationFactory<BaseStartup> factory, ITestOutputHelper output )
            : base( factory, output ) {
        }

        [Fact]
        public async void Get_all_weather_forecast_ok( ) {

            #region [ Arrange ]

            #endregion [ Arrange ]

            #region [ Act ]

            var response = await _client.GetAsync( $"{ApiUrl}/{EndpointUrl}" );

            #endregion [ Act ]

            #region [ Assert ]

            await EnsureSuccessAsync( response );
            var weatherForecast = await response.GetResponseAsync<IEnumerable<WeatherForecastViewModel>>( );

            Assert.NotNull( weatherForecast );

            #endregion [ Assert ]
        }

        [Fact]
        public async void Get_first_weather_forecast_ok( ) {

            #region [ Arrange ]

            #endregion [ Arrange ]

            #region [ Act ]

            var response = await _client.GetAsync( $"{ApiUrl}/{EndpointUrl}/1" );

            #endregion [ Act ]

            #region [ Assert ]

            await EnsureSuccessAsync( response );
            var weatherForecast = await response.GetResponseAsync<WeatherForecastViewModel>( );

            Assert.NotNull( weatherForecast );

            #endregion [ Assert ]
        }
    }
}
