﻿using System.Collections.Generic;

namespace Presentations.Api.Application.ViewModels {
    public class ErrorValidationResponse {
        public IEnumerable<MessageValidationResponse> Erros { get; set; }
    }
}