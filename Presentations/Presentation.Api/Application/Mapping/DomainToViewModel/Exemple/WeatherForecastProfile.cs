﻿using AutoMapper;
using Presentations.Api.Application.ViewModels.Exemple;
using Exemple.Domain.AggregateModels;

namespace Presentations.Api.Application.Mapping.DomainToViewModel.Exemple {
    public class WeatherForecastProfile: Profile {
        public WeatherForecastProfile( ) {
            CreateMap<WeatherForecast, WeatherForecastViewModel>( );
        }
    }
}
