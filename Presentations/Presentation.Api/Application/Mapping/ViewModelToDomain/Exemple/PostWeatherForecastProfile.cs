﻿using AutoMapper;
using Presentations.Api.Application.ViewModels.Exemple;
using Exemple.Domain.Commands;

namespace Presentations.Api.Application.Mapping.ViewModelToDomain.Exemple {
    public class PostWeatherForecastProfile: Profile {
        public PostWeatherForecastProfile( ) {
            CreateMap<PostWeatherForecastViewModel, PostWeatherForecastCommand>( );
        }
    }
}
