﻿using AutoMapper;
using Base.Domain.Events;
using Base.Domain.Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Presentations.Api.Application.IntegrationEventsHandlers {
    public abstract class BaseIntegrationEventHandler<TIntegrationEvent>: INotificationHandler<TIntegrationEvent> where TIntegrationEvent : Event {
        protected readonly IMediatorHandler _mediator;
        protected readonly IMapper _mapper;

        public abstract Task Handle( TIntegrationEvent notification, CancellationToken cancellationToken );

        public BaseIntegrationEventHandler( IMediatorHandler mediator, IMapper mapper ) {
            _mediator = mediator;
            _mapper = mapper;
        }
    }
}