using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using System.IO;

namespace Presentations.Api {
    public class Program {
        public static void Main( string[ ] args ) {
            CreateWebHostBuilder( args ).Build( ).Run( );
        }

        private static IWebHostBuilder CreateWebHostBuilder( string[ ] args ) =>
            WebHost.CreateDefaultBuilder( args )
                .UseContentRoot( Directory.GetCurrentDirectory( ) )
                .UseStartup<Startup>( )
                .ConfigureLogging( ( hostingContext, logging ) => {
                    logging.AddConsole( );
                    logging.AddDebug( );
                    logging.AddSerilog( );
                } )
                .UseSerilog( ( hostingContext, loggerConfiguration ) => loggerConfiguration
                     .ReadFrom.Configuration( hostingContext.Configuration ) );
    }
}
