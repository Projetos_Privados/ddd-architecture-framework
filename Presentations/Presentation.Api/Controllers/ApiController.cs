﻿using AutoMapper;
using Presentations.Api.Application.ViewModels;
using Base.Domain.EventHandlers;
using Base.Domain.Events;
using Base.Domain.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Presentations.Api {
    [Route( "api/[controller]" )]
    [ProducesResponseType( typeof( ExeceptionResponse ), StatusCodes.Status500InternalServerError )]
    public abstract class ApiController: ControllerBase {
        protected readonly DomainNotificationHandler _notifications;
        protected readonly IMediatorHandler _mediator;
        protected readonly IResponseFactory _responseFactory;
        protected readonly IMapper _mapper;
        protected readonly IServiceProvider _serviceProvider;

        protected ApiController( INotificationHandler<DomainNotification> notifications,
                                IMediatorHandler mediator,
                                IMapper mapper,
                                IServiceProvider serviceProvider ) {
            _notifications = ( DomainNotificationHandler ) notifications;
            _mediator = mediator;
            _mapper = mapper;
            _serviceProvider = serviceProvider;
        }

        protected IEnumerable<DomainNotification> Notifications => _notifications.GetNotifications( );

        protected new IActionResult Response( object result ) {
            if ( !_notifications.HasNotifications( ) ) {
                return Ok( result );
            }

            return BadRequest( new ErrorValidationResponse {
                Erros = _notifications.GetNotifications( ).Select( x => new MessageValidationResponse( x.PropertyName, x.ErrorMessage ) )
            } );
        }

        protected IActionResult ResponseRedirect( string actionName, object routeValues, object value ) {
            if ( !_notifications.HasNotifications( ) ) {
                return CreatedAtAction( actionName, routeValues, value );
            }

            return BadRequest( new ErrorValidationResponse {
                Erros = _notifications.GetNotifications( ).Select( x => new MessageValidationResponse( x.PropertyName, x.ErrorMessage ) )
            } );
        }

        protected IActionResult ResponseRedirect( string actionName, string controllerName, object routeValues, object value ) {
            if ( !_notifications.HasNotifications( ) ) {
                return CreatedAtAction( actionName, controllerName, routeValues, value );
            }

            return BadRequest( new ErrorValidationResponse {
                Erros = _notifications.GetNotifications( ).Select( x => new MessageValidationResponse( x.PropertyName, x.ErrorMessage ) )
            } );
        }
    }

    public interface IResponseFactory {
        IResponse GetResponse( string httpMethods );

        IActionResult GetResult( string httpMethod );
    }

    public class ResponseFactory: IResponseFactory {
        protected readonly DomainNotificationHandler _notifications;
        protected readonly IMediatorHandler _mediator;

        public ResponseFactory( INotificationHandler<DomainNotification> notifications,
                               IMediatorHandler mediator ) {
            _notifications = ( DomainNotificationHandler ) notifications;
            _mediator = mediator;
        }

        public IResponse GetResponse( string httpMethod ) {
            if ( httpMethod == HttpMethods.Get ) {
                return new GetResponse( );
            }

            if ( httpMethod == HttpMethods.Put ) {
                return new PutResponse( );
            }

            if ( httpMethod == HttpMethods.Post ) {
                return new PostResponse( );
            }

            if ( httpMethod == HttpMethods.Delete ) {
                return new DeleteResponse( );
            }

            return null;
        }

        public IActionResult GetResult( string httpMethod ) {
            if ( !_notifications.HasNotifications( ) ) {
                var response = GetResponse( httpMethod );
                return response.Get( );
            }

            return new BadRequestObjectResult( new ErrorValidationResponse {
                Erros = _notifications.GetNotifications( ).Select( x => new MessageValidationResponse( x.PropertyName, x.ErrorMessage ) )
            } );
        }
    }

    public interface IResponse {
        IActionResult Get( );
        IActionResult Get( object result );
    }

    public class PutResponse: IResponse {
        public IActionResult Get( ) {
            return new NoContentResult( );
        }

        public IActionResult Get( object result ) {
            return new OkObjectResult( result );
        }
    }

    public class GetResponse: IResponse {
        public IActionResult Get( ) {
            throw new NotImplementedException( );
        }

        public IActionResult Get( object result ) {
            return new OkObjectResult( result );
        }
    }

    public class PostResponse: IResponse {
        public IActionResult Get( ) {
            throw new NotImplementedException( );
        }

        public IActionResult Get( object result ) {
            throw new NotImplementedException( );
        }
    }

    public class DeleteResponse: IResponse {
        public IActionResult Get( ) {
            return new NoContentResult( );
        }

        public IActionResult Get( object result ) {
            return new NoContentResult( );
        }
    }
}