﻿using AutoMapper;
using Base.Domain.Events;
using Base.Domain.Interfaces;
using Base.Infrastructure.CrossCutting.Transaction;
using Exemple.Domain.AggregateModels;
using Exemple.Domain.Commands;
using Exemple.Domain.Interfaces.Queries;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using Presentations.Api.Application.ViewModels.Exemple;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Presentations.Api.Controllers {
    [Route( "api/Exemple/" )]
    [OpenApiTags( "Exemple" )]
    public class WeatherForecastController: ApiController {

        private readonly IWeatherForecastQuery _weatherForecastQuery;

        public WeatherForecastController(
            INotificationHandler<DomainNotification> notifications,
            IMediatorHandler mediator,
            IMapper mapper,
            IServiceProvider serviceProvider,
            IWeatherForecastQuery weatherForecastQuery ) : base( notifications, mediator, mapper, serviceProvider ) {
            _weatherForecastQuery = weatherForecastQuery;
        }

        [HttpGet( "[controller]" )]
        [OpenApiOperation( "Get all weather forecast", "Return all forecasts save in database" )]
        [ProducesResponseType( typeof( IEnumerable<WeatherForecastViewModel> ), StatusCodes.Status200OK )]
        [ProducesResponseType( StatusCodes.Status404NotFound )]
        public async Task<IActionResult> GetAsync( CancellationToken cancellationToken ) {
            var weatherForecasts = await _weatherForecastQuery.GetAsync( cancellationToken );
            var result = _mapper.Map<IEnumerable<WeatherForecastViewModel>>( weatherForecasts );
            return Response( result );
        }

        [HttpGet( "[controller]/{id}" )]
        [OpenApiOperation( "Get single weather forecast", "Return forecast by id" )]
        [ProducesResponseType( typeof( WeatherForecastViewModel ), StatusCodes.Status200OK )]
        [ProducesResponseType( StatusCodes.Status404NotFound )]
        public async Task<IActionResult> GetAsync( [FromRoute] long id, CancellationToken cancellationToken ) {
            var weatherForecasts = await _weatherForecastQuery.GetAsync( id, cancellationToken );
            var result = _mapper.Map<WeatherForecastViewModel>( weatherForecasts );
            return Response( result );
        }

        [HttpPost( "[controller]" )]
        [OpenApiOperation( "Post weather forecast", "Create a new forecast" )]
        [ProducesResponseType( typeof( WeatherForecastViewModel ), StatusCodes.Status200OK )]
        [ProducesResponseType( StatusCodes.Status404NotFound )]
        public async Task<IActionResult> PostAsync( [FromBody] PostWeatherForecastViewModel postWeatherForecast, CancellationToken cancellationToken ) {
            using var transaction = new Transactions( _serviceProvider );
            var command = _mapper.Map<PostWeatherForecastCommand>( postWeatherForecast );
            var result = await _mediator.SendCommandAsync<PostWeatherForecastCommand, WeatherForecast>( command, cancellationToken );
            var response = _mapper.Map<WeatherForecast, WeatherForecastViewModel>( result );

            if ( result != null )
                transaction.Commit( );

            return ResponseRedirect( "Get", new { id = response?.WeatherForecastId }, response );
        }

    }
}
