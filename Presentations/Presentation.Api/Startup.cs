using Presentations.Api.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Logging;

namespace Presentations.Api {
    public class Startup: BaseStartup {

        public Startup( IConfiguration configuration, ILogger<Startup> logger ) : base( configuration, logger ) {
        }

        public override void ConfigureServices( IServiceCollection services ) {
            IdentityModelEventSource.ShowPII = true;

            services.UseSwaggerMiddleware( );

            services.AddCors( options => {
                options.AddPolicy( "CorsPolicy",
                    builder => builder
                    .SetIsOriginAllowed( ( host ) => true )
                    .AllowAnyMethod( )
                    .AllowAnyHeader( )
                    .AllowCredentials( ) );
            } );

            base.ConfigureServices( services );
        }

        public override void Configure( IApplicationBuilder app, IWebHostEnvironment env ) {
            if ( env.IsDevelopment( ) ) {
                app.UseDeveloperExceptionPage( );
            }

            app.UseCors( "CorsPolicy" );

            app.ConfigureSwagger( );

            app.UseRouting( );

            app.UseAuthorization( );

            app.UseEndpoints( endpoints => {
                endpoints.MapControllers( );
            } );

            base.Configure( app, env );
        }
    }
}
