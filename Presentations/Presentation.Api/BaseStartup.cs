﻿using AutoMapper;
using Base.Infrastructure.CrossCutting.Environment;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Presentations.Api.Conventions;
using Presentations.Api.Extensions;
using System;
using System.Globalization;
using BaseIoC = Base.Infrastructure.CrossCutting.IoC;
using ExempleIoC = Exemple.Infrastructure.CrossCutting.IoC;


namespace Presentations.Api {
    public class BaseStartup {

        public IConfiguration Configuration { get; }
        private readonly ILogger<BaseStartup> _logger;

        public BaseStartup( IConfiguration configuration, ILogger<Startup> logger ) {
            Configuration = configuration;
            _logger = logger;
        }

        public virtual void ConfigureServices( IServiceCollection services ) {
            IdentityModelEventSource.ShowPII = true;

            SetCulture( );

            services
                .AddControllers( opts =>
                    opts.Conventions.Add( new CommaSeparatedRouteConvention( ) )
                 )
                .SetCompatibilityVersion( CompatibilityVersion.Latest )
                .AddNewtonsoftJson( options => {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver( );
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                } );

            RegisterContainers( services );
        }

        public virtual void Configure( IApplicationBuilder app, IWebHostEnvironment env ) {
            app.UseExceptionMiddleware( _logger );
            app.UseHttpsRedirection( );
        }

        protected static void SetCulture( string culture = "pt-BR" ) {
            var cultureInfo = new CultureInfo( culture );
            CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
            CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;
        }

        protected void RegisterContainers( IServiceCollection services ) {

            #region [ AutoMapper ]

            var mappingConfig = new MapperConfiguration( mc => {
                mc.AddMaps( new[ ] {
                   this.GetType().Namespace
                } );
            } );

            IMapper mapper = mappingConfig.CreateMapper( );
            services.AddSingleton( mapper );
            mappingConfig.AssertConfigurationIsValid( );

            #endregion [ AutoMapper ]

            #region [ MediatR ]

            services.AddMediatR( typeof( BaseStartup ) );

            #endregion [ MediatR ]

            #region [ Connection String ]

            var connectionString = Configuration.GetConnectionString( "DefaultConnection" );
            connectionString = connectionString.Replace( "%APPDATA%", Environment.GetFolderPath( Environment.SpecialFolder.ApplicationData ) );

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>( );

            #endregion [ Connection String ]

            #region [ Injectors ]

            var settings = Configuration.GetSection( @"Services" ).Get<AppSetttings>( );
            BaseIoC.InjectorContainer.Register( services, settings, connectionString );
            ExempleIoC.InjectorContainer.Register( services, connectionString );

            #endregion [ Injectors ]
        }
    }
}
