﻿using Base.Domain.Interfaces;
using Base.Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Base.Infrastructure.Data.Repository.Repositories {
    public class ReadRepository<T>: IReadRepository<T> where T : class {
        private readonly BaseContext _context;

        public ReadRepository( BaseContext context ) {
            _context = context;
        }

        public virtual ValueTask<T> FindAsync( CancellationToken cancellationToken, params object[ ] keys ) {
            var entity = _context.Set<T>( ).FindAsync( keys, cancellationToken );
            return entity;
        }

        public virtual Task<List<T>> SearchAsync( ISpecification<T> specification, CancellationToken cancellationToken ) {
            var result = specification.Prepare( _context.Set<T>( ).AsQueryable( ) )
                                      .ToListAsync( cancellationToken );
            return result;
        }

        public virtual Task<List<T>> SearchAsync( ISpecification<T> specification, int pageNumber, int pageSize, CancellationToken cancellationToken ) {
            var query = specification.Prepare( _context.Set<T>( ).AsQueryable( ) )
                                     .Skip( pageNumber )
                                     .Take( pageSize );

            var entities = query.ToListAsync( cancellationToken );
            return entities;
        }

        public virtual IQueryable<T> Where( ISpecification<T> specification ) {
            var query = _context.Set<T>( ).Where( specification.Predicate );
            return query;
        }

        public virtual Task<int> CountAsync( ISpecification<T> specification, CancellationToken cancellationToken ) {
            var result = _context.Set<T>( )
                                  .CountAsync( specification.Predicate, cancellationToken );
            return result;
        }

        public virtual Task<bool> AnyAsync( ISpecification<T> specification, CancellationToken cancellationToken ) {
            var result = _context.Set<T>( )
                                  .AnyAsync( specification.Predicate, cancellationToken );
            return result;
        }

        public virtual async Task<T> FirstOrDefaultAsync( ISpecification<T> specification, CancellationToken cancellationToken ) {
            return await _context.Set<T>( )
                                  .FirstOrDefaultAsync( specification.Predicate, cancellationToken ).ConfigureAwait( false );
        }

        public virtual IQueryable<T> AsQueryable( ) {
            return _context.Set<T>( )
                            .AsQueryable( );
        }

        public virtual Task<bool> AllAsync( ISpecification<T> specification, CancellationToken cancellationToken ) {
            var result = _context.Set<T>( )
                                  .AllAsync( specification.Predicate, cancellationToken );
            return result;
        }

        public virtual Task<List<T>> ToListAsync( CancellationToken cancellationToken ) {
            return _context.Set<T>( ).ToListAsync( );
        }

        public string GetProviderName( ) {
            return _context.Database.ProviderName;
        }
    }
}