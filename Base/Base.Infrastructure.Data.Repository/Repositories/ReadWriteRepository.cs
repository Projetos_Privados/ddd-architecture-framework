﻿using Base.Domain.Interfaces;
using Base.Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Base.Infrastructure.Data.Repository.Repositories {
    public class ReadWriteRepository<T>: IReadWriteRepository<T> where T : class {
        private readonly BaseContext _context;

        public ReadWriteRepository( BaseContext context ) {
            _context = context;
        }

        public virtual ValueTask<T> FindAsync( CancellationToken cancellationToken, params object[ ] keys ) {
            var entity = _context.Set<T>( ).FindAsync( keys, cancellationToken );
            return entity;
        }

        public virtual Task<List<T>> SearchAsync( ISpecification<T> specification, CancellationToken cancellationToken ) {
            var result = specification.Prepare( _context.Set<T>( ).AsQueryable( ) )
                                      .ToListAsync( cancellationToken );
            return result;
        }

        public virtual Task<List<T>> SearchAsync( ISpecification<T> specification, int pageNumber, int pageSize, CancellationToken cancellationToken ) {
            var query = specification.Prepare( _context.Set<T>( ).AsQueryable( ) )
                                     .Skip( pageNumber )
                                     .Take( pageSize );

            var entities = query.ToListAsync( cancellationToken );
            return entities;
        }

        public virtual Task<List<T>> ToListAsync( CancellationToken cancellationToken ) {
            return _context.Set<T>( ).ToListAsync( );
        }

        public virtual IQueryable<T> Where( ISpecification<T> specification ) {
            var query = _context.Set<T>( ).Where( specification.Predicate );
            return query;
        }

        public virtual Task<int> CountAsync( ISpecification<T> specification, CancellationToken cancellationToken ) {
            var result = _context.Set<T>( )
                                  .CountAsync( specification.Predicate, cancellationToken );
            return result;
        }

        public Task<bool> AnyAsync( ISpecification<T> specification, CancellationToken cancellationToken ) {
            var result = _context.Set<T>( )
                                  .AnyAsync( specification.Predicate, cancellationToken );
            return result;
        }

        public virtual Task<T> FirstOrDefaultAsync( ISpecification<T> specification, CancellationToken cancellationToken ) {
            var result = _context.Set<T>( )
                                  .FirstOrDefaultAsync( specification.Predicate, cancellationToken );
            return result;
        }

        public virtual IQueryable<T> AsQueryable( ) {
            return _context.Set<T>( )
                            .AsQueryable( );
        }

        public virtual T Add( T entity ) {
            var result = _context.Set<T>( ).Add( entity );
            return result.Entity;
        }

        public virtual Task AddRange( IEnumerable<T> entity, CancellationToken cancellationToken ) {
            var result = _context.Set<T>( ).AddRangeAsync( entity, cancellationToken );
            return result;
        }

        public virtual void Remove( T entity ) {
            _context.Set<T>( ).Remove( entity );
        }

        public virtual void Update( T entity ) {
            Attach( entity );
            _context.Entry( entity ).State = EntityState.Modified;
        }

        public void Attach( T entity ) {
            if ( _context.Entry( entity ).State == EntityState.Detached ) {
                _context.Set<T>( ).Attach( entity );
            }
        }

        public Task<int> SaveChangesAsync( CancellationToken cancellationToken ) {
            return _context.SaveChangesAsync( cancellationToken );
        }

        public Task<bool> AllAsync( ISpecification<T> specification, CancellationToken cancellationToken ) {
            var result = _context.Set<T>( )
                                  .AllAsync( specification.Predicate, cancellationToken );
            return result;
        }

        public virtual string GetProviderName( ) {
            return _context.Database.ProviderName;
        }
    }
}