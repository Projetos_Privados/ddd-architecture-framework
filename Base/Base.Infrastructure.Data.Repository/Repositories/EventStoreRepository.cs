﻿using Base.Domain.Events;
using Base.Domain.Interfaces;
using Base.Infrastructure.Data.Context;

namespace Base.Infrastructure.Data.Repository.Repositories {
    public class EventStoreRepository: ReadWriteRepository<EventLog>, IEventStoreRepository {
        public EventStoreRepository( EventStoreContext context )
            : base( context ) {
        }
    }
}