﻿using Base.Domain.EventHandlers;
using Base.Domain.Events;
using Base.Domain.Interfaces;
using Base.Infrastructure.CrossCutting.Bus;
using Base.Infrastructure.CrossCutting.Environment;
using Base.Infrastructure.Data.Context;
using Base.Infrastructure.Data.Repository.Repositories;
using MediatR;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

namespace Base.Infrastructure.CrossCutting.IoC {
    public static class InjectorContainer {
        public static void Register( IServiceCollection services, AppSetttings appSetttings, string connectionString ) {
            services.AddScoped( ( x ) => appSetttings );
            services.AddScoped<IEventStore, EventStore>( );
            services.AddScoped<IMediatorHandler, InMemoryBus>( );
            services.AddScoped<IEventStoreRepository, EventStoreRepository>( );
            services.AddScoped<INotificationHandler<DomainNotification>, DomainNotificationHandler>( );

            if ( !string.IsNullOrEmpty( connectionString ) ) {
                services.AddScoped( ( serviceProvider ) => {
                    var dbConnection = new SqliteConnection( connectionString );
                    dbConnection.Open( );
                    return dbConnection;
                } );

                services.AddScoped<DbTransaction>( ( serviceProvider ) => {
                    var dbConnection = serviceProvider.GetService<SqliteConnection>( );
                    return dbConnection.BeginTransaction( IsolationLevel.ReadCommitted );
                } );

                services.AddScoped( ( serviceProvider ) => {
                    var dbConnection = serviceProvider.GetService<SqliteConnection>( );

                    var dbContext = new DbContextOptionsBuilder( )
                    .UseSqlite( dbConnection, x => {
                        x.MigrationsHistoryTable( "migrations_history" );
                        x.MigrationsAssembly( "Base.Infrastructure.Data.Migrations" );
                    } )
                    .UseLazyLoadingProxies( )
                    .EnableSensitiveDataLogging( );

                    AddLogContext( dbContext );

                    return dbContext.Options;
                } );
            }

            InjectContext<EventStoreContext>( services, connectionString );
        }

        public static void InjectContext<TContext>( IServiceCollection services, string connectionString ) where TContext : BaseContext {
            if ( string.IsNullOrWhiteSpace( connectionString ) ) {
                var serviceProvider = new ServiceCollection( )
                    .AddEntityFrameworkInMemoryDatabase( )
                    .AddEntityFrameworkProxies( )
                    .BuildServiceProvider( );

                services.AddDbContext<TContext>( options => options
                     .UseInternalServiceProvider( serviceProvider )
                     .UseInMemoryDatabase( "application_memory_db" )
                     .UseLazyLoadingProxies( )
                     .EnableSensitiveDataLogging( ) );
            } else {
                services.AddScoped( ( serviceProvider ) => {
                    var transaction = serviceProvider.GetService<DbTransaction>( );
                    var options = serviceProvider.GetService<DbContextOptions>( );

                    var context = ( TContext ) Activator.CreateInstance( typeof( TContext ), options );
                    context.Database.UseTransaction( transaction );
                    return context;
                } );
            }
        }

        private static ILoggerFactory GetLoggerFactory( ) {
            IServiceCollection serviceCollection = new ServiceCollection( );
            serviceCollection.AddLogging( builder =>
                    builder.AddConsole( )
                           .AddFilter( DbLoggerCategory.Database.Command.Name,
                                      LogLevel.Information ) );
            return serviceCollection.BuildServiceProvider( )
                    .GetService<ILoggerFactory>( );
        }

        [Conditional( "DEBUG" )]
        private static void AddLogContext( DbContextOptionsBuilder dbContext ) {
            dbContext.UseLoggerFactory( GetLoggerFactory( ) );
            //.EnableSensitiveDataLogging();
        }
    }
}