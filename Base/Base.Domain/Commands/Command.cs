﻿using Base.Domain.Events;

namespace Base.Domain.Commands
{
    public class Command<TResponse> : Message<TResponse>
    {
        protected Command()
        {
        }
    }
}