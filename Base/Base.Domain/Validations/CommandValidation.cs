﻿using Base.Domain.Commands;
using FluentValidation;

namespace Base.Domain.Validations {
    public abstract class CommandHandlerValidation<TCommand, TResponse>: AbstractValidator<TCommand> where TCommand : Command<TResponse> {
    }
}