﻿using Base.Domain.Events;

namespace Base.Domain.Interfaces
{
    public interface IEventStoreRepository : IReadWriteRepository<EventLog>
    {
    }
}