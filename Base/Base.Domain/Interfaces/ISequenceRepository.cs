﻿using System.Threading;
using System.Threading.Tasks;

namespace Base.Domain.Interfaces
{
    public interface ISequenceRepository
    {
        Task<long> ObterProxima(string sequenceName, CancellationToken cancellationToken);
    }
}