﻿using Base.Domain.Events;
using MediatR;
using System;
using System.Collections.Generic;

namespace Base.Domain.Interfaces
{
    public interface IDomainNotificationHandler : INotificationHandler<DomainNotification>, IDisposable
    {
        List<DomainNotification> GetNotifications();

        bool HasNotifications();
    }
}