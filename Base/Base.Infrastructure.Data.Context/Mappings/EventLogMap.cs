﻿using Base.Domain.Events;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Base.Infrastructure.Data.Context.Mappings {
    internal class EventLogMap: IEntityTypeConfiguration<EventLog> {
        public void Configure( EntityTypeBuilder<EventLog> builder ) {
            
            builder.ToTable( "log_event_domain" )
                .HasKey( x => x.Id)
                .HasName( "event_log_id");

            builder.Property( x => x.Id )
                   .HasMaxLength( 32 )
                   .HasColumnName( "event_log_id" );

            builder.Property( x => x.Content )
                   .HasColumnName( "content" )
                   .HasMaxLength( 4000 )
                   .IsRequired( );

            builder.Property( x => x.CreationDate )
                   .HasColumnName( "created_at" )
                   .IsRequired( );

            builder.Property( x => x.EventTypeName )
                  .HasColumnName( "type_name" )
                  .HasMaxLength( 150 )
                  .IsRequired( );

            builder.Property( x => x.State )
                  .HasColumnName( "state" )
                  .IsRequired( );
        }
    }
}