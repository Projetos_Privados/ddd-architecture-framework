﻿using Microsoft.EntityFrameworkCore;

namespace Base.Infrastructure.Data.Context {
    public abstract class BaseContext: DbContext {
        public BaseContext( DbContextOptions options )
            : base( options ) {
        }

        protected override void OnModelCreating( ModelBuilder modelBuilder ) {
            // SQLite not suport schema
            //modelBuilder.HasDefaultSchema( "spartan" );
        }
    }
}