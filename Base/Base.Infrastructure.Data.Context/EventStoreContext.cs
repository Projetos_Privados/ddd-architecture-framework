﻿using Base.Domain.Events;
using Base.Infrastructure.Data.Context.Mappings;
using Microsoft.EntityFrameworkCore;

namespace Base.Infrastructure.Data.Context {
    public class EventStoreContext: BaseContext {
        public EventStoreContext( DbContextOptions options )
           : base( options ) {
        }

        public DbSet<EventLog> EventLog { get; set; }

        protected override void OnModelCreating( ModelBuilder modelBuilder ) {
            modelBuilder.ApplyConfiguration( new EventLogMap( ) );

            base.OnModelCreating( modelBuilder );
        }
    }
}