﻿using Base.Domain.Events;
using Base.Domain.Interfaces;
using Base.Infrastructure.Data.Context;
using Base.Test.Mock;
using MediatR;
using System.Threading.Tasks;

namespace Base.Test.Commands {
    public class CommandTestBase {
        protected readonly BaseContext _context;

        protected readonly IMediatorHandler _bus;
        protected readonly INotificationHandler<DomainNotification> _domainNotificationHandler;

        public CommandTestBase( BaseContext context ) {
            _context = context;

            _domainNotificationHandler = MockDomainNotificationHandler.Get( );
            _bus = MockMediatorHandler.Get( );
        }

        protected async Task LimparContexto( ) {
            await _context.Database.EnsureDeletedAsync( );
        }

        protected async Task SalvarContexto( ) {
            await _context.SaveChangesAsync( );
        }
    }
}