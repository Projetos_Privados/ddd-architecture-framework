﻿using Base.Domain.Events;
using Base.Domain.Interfaces;
using Moq;
using System.Threading;
using System.Threading.Tasks;

namespace Base.Test.Mock {
    public static class MockMediatorHandler {
        public static IMediatorHandler Get( ) {
            var mediatorHandler = new Mock<IMediatorHandler>( );
            mediatorHandler.Setup( x => x.RaiseEventAsync( It.IsAny<Event>( ), It.IsAny<CancellationToken>( ) ) )
                           .Returns( Task.FromResult<Event>( null ) );

            return mediatorHandler.Object;
        }
    }
}