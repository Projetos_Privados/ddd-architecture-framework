﻿using Base.Domain.Events;
using Base.Domain.ValueObjects;
using System;
using Xunit;

namespace Base.Test.Domain.Aggregates {
    public class EventLogTest {
        [Fact]
        public void Event_log_must_be_created( ) {
            var timestamp = DateTime.Today;
            var fullName = typeof( EventLogTest ).FullName;
            var content = "content";

            var @event = new EventLog( Guid.NewGuid( ), timestamp, fullName, content );

            Assert.Equal( @event.CreationDate, timestamp );
            Assert.Equal( EventStateEnum.NotPublished, @event.State );
            Assert.Equal( @event.Content, content );
            Assert.NotEqual( @event.Id, Guid.Empty );
            Assert.Equal( @event.EventTypeName, fullName );
        }

        [Fact]
        public void Event_log_must_be_changed_to_published( ) {
            var timestamp = DateTime.Today;
            var fullName = typeof( EventLogTest ).FullName;
            var content = "content";

            var @event = new EventLog( Guid.NewGuid( ), timestamp, fullName, content );

            @event.MarkEventAsPublished( );

            Assert.True( @event.State.HasFlag( EventStateEnum.NotPublished ) );
            Assert.True( @event.State.HasFlag( EventStateEnum.Published ) );
        }

        [Fact]
        public void Event_log_must_be_changed_to_fail_published( ) {
            var timestamp = DateTime.Today;
            var fullName = typeof( EventLogTest ).FullName;
            var content = "content";

            var @event = new EventLog( Guid.NewGuid( ), timestamp, fullName, content );

            @event.MarkEventAsFailed( );

            Assert.True( @event.State.HasFlag( EventStateEnum.PublishedFailed ) );
        }

        [Fact]
        public void Event_log_must_be_changed_to_processed( ) {
            var timestamp = DateTime.Today;
            var fullName = typeof( EventLogTest ).FullName;
            var content = "content";

            var @event = new EventLog( Guid.NewGuid( ), timestamp, fullName, content );

            @event.MarkEventAsPublished( );
            @event.MarkEventAsProcessed( );

            Assert.True( @event.State.HasFlag( EventStateEnum.NotPublished ) );
            Assert.True( @event.State.HasFlag( EventStateEnum.Published ) );
            Assert.True( @event.State.HasFlag( EventStateEnum.Processed ) );
        }

        [Fact]
        public void Event_log_cant_be_changed_to_processed( ) {
            var timestamp = DateTime.Today;
            var fullName = typeof( EventLogTest ).FullName;
            var content = "content";

            var @event = new EventLog( Guid.NewGuid( ), timestamp, fullName, content );

            @event.MarkEventAsProcessed( );

            Assert.False( @event.State.HasFlag( EventStateEnum.Processed ) );
        }
    }
}