﻿using System;

namespace Base.Infrastructure.CrossCutting.Transaction.Interfaces
{
    public interface ITransactions : IDisposable
    {
        void Commit();
    }
}